var express = require('express');
var router = express.Router();
var ponddtlsController = require('../../controllers/ponddtlsController');



router.post('/updatePonddtls',function(req,res){
    ponddtlsController.updateLoactionDtls(req.body,function(response){
        res.send(response);
    });
});


router.get('/getPondDtls/:id',function(req,res){
    ponddtlsController.getPondDtls(req.params.id,function(response){
        res.send(response);
    });
});

module.exports = router;
