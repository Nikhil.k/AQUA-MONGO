var express = require('express');
var router = express.Router();
var usermstController = require('../../controllers/usermstController');

router.post('/insertUserData',function(req,res){
  usermstController.insertUserData(req.body,function(response){
      res.send(response);
  });
});

router.post('/userLogin',function(req,res){
  usermstController.userLogin(req.body,function(response){
      res.send(response);
  });
});

router.get('/isFirstLogin/:id',function(req,res){
  usermstController.isFirstLogin(req.params.id,function(response){
      res.send(response);
  });
});
module.exports = router;
