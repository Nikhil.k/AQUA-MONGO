"use strict"

var express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    verifyToken = require('../../config/auth').tokenAuth,
    jwt = require('jsonwebtoken'),
    config = require('../../config/config'),
    user_api = require('./usermstRoutes'),
    gateway_api = require('./gatewayRoutes'),
    login_api = require('./loginRoutes'),
    pond_api = require('./ponddtlsRoutes'),
    sensor_api = require('./sensorinfoRoutes');





router.post('/token', function (req, res, next) {
    passport.authenticate('local', function (err, userObj, info) {
        if (err) {
            return next(err);
        }
        if (!userObj) {
            return res.status(401).json(info);
        }
        var token = jwt.sign(
            { email: JSON.stringify(userObj.email) },
            config.auth.tokenSecret,
            { expiresIn: '1 days' }
        );
        return res.json({ token: token });
    })(req, res, next);
});

router.get('/me', passport.authenticate('jwt', { session: false }), function (req, res) {
    res.send({ user_info: req.user });
});

router.use('/user', user_api);
router.use('/gatewayRoutes', gateway_api);
router.use('/loginRoutes', login_api);
router.use('/ponddtlsRoutes', pond_api);
router.use('/sensorinfoRoutes', sensor_api);


module.exports = router;