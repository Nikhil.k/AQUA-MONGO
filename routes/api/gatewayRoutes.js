var express = require('express');
var router = express.Router();
var gatewayController = require('../../controllers/gatewayController');



router.post('/insertPondOrGatewayDtls',function(req,res){
    gatewayController.insertPondOrGatewayDtls(req.body,function(response){
        res.send(response);
    });
});

router.get('/getGatewayDtls/:pondid',function(req,res){
    gatewayController.getGatewayDtls(req.params.pondid,function(response){
        res.send(response);
    });
});


module.exports = router;
