var ponddtlsModel = require('../models/ponddtlsModel.js'),
    gatewayModel=require('../models/gatewayModel');

function pondService(){

  /**
   * updateLoactionDtls:The below function 
   * is used to update location details for
   * a particular pond 
   * @param:userid
   * REST_URL:http://localhost:8080/aqua/api/ponddtlsRoutes/updateLoactionDtls
   */

  this.updateLoactionDtls=function(userData,callback){
    ponddtlsModel.update({usermstid:userData.usermstid},userData,(err,updatePond)=>{
         callback(updatePond);
     });  
  }

  /**
   * getPondDtls:The below function 
   * is used to retrieve Pond details for
   * a particular User 
   * @param:userid
   * REST_URL:http://localhost:8080/aqua/api/ponddtlsRoutes/getPondDtls
   */

  this.getPondDtls=function(userid,callback){
    let query=ponddtlsModel.find({}).select('location');
    query.where({usermstid:userid});
    query.exec((err,pondDtlsResp)=>{
      callback(pondDtlsResp[0])
    })
  }

  
  

}
pondService=new pondService();

module.exports=pondService;