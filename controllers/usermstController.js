var usermstModel = require('../models/usermstModel'),
    md5 = require('md5'),
    loginMstModel = require('../models/loginModel');

function userService() {

 /**
   * insertUserData:The below function 
   * is used to insert user details 
   * @param:JSON Object which includes personal details
   * of user
   * REST_URL:http://localhost:8080/aqua/api/user/insertUserData
   */

    this.insertUserData = function (userData, callback) {
        userData.password = md5(userData.password);
        const userMstModel = new usermstModel(userData);
        userMstModel.save(function (err, response) {
            if (err) throw err;
            callback(response);
        });
    }

 /**
   * userLogin:The below function 
   * is used to check's whether  user is
   * registered user or NOT and inserts data
   * into LOGIN collection to maintain logged in details
   * @param:JSON Object which includes username and password
   * REST_URL:http://localhost:8080/aqua/api/user/userLogin
   */

    this.userLogin = function (userCredentials, callback) {
        let userPassword = md5(userCredentials.password);
        let query = usermstModel.find({}).select('_id');
        query.where({ password: userPassword, username: userCredentials.username });
        query.exec((err, loginResp) => {
            if (loginResp[0] != null && loginResp[0] != undefined && loginResp[0] != "") {
                userCredentials.usermstid = loginResp[0]._id;
                userCredentials.lastlogindatetime = new Date();
                userCredentials.password = userPassword;
                let login = new loginMstModel(userCredentials);
                login.save((err, loggedInData) => {
                    callback(loggedInData);
                })
            }
            else {
                callback(loginResp);
            }

        });
    }

 /**
   * isFirstLogin:The below function 
   * is used to check whether the user is Logged in
   * for the first time or NOT 
   * @param:userid
   * @returns:true if user is Logged In 
   * REST_URL:http://localhost:8080/aqua/api/user/isFirstLogin
   */

    this.isFirstLogin = function (userid, callback) {
        let query = loginMstModel.find({}).select('_id');
        query.where({ usermstid: userid });
        query.exec((err, loggedInResp) => {
            let isLoggedIn;
           if(loggedInResp[0]!=null && loggedInResp[0]!=undefined && loggedInResp[0]!="" ){
               isLoggedIn=true;
           }
           else{
               isLoggedIn=false;
           }
            callback(isLoggedIn);
        })
    }


}
userService = new userService();

module.exports = userService;