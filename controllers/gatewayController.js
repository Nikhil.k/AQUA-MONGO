var gatewayModel = require('../models/gatewayModel'),
    ponddtlsModel = require('../models/ponddtlsModel');


function gatewayService() {

 /**
   * insertPondandGatewayDtls:The below function 
   * is used to insert pond details with
   * respect to its gateway details
   * @param:JSON object which includes location and
   * gateway details of particular pond
   * REST_URL:http://localhost:8080/aqua/api/gatewayRoutes/insertPondOrGatewayDtls
   */

    this.insertPondandGatewayDtls = function (gatewayData, callback) {
        const ponddtls=new ponddtlsModel(gatewayData);
        ponddtls.save((err,ponddtlsRes)=>{
            gatewayData.ponddtlsid=ponddtlsRes._id;
            const gateway=new gatewayModel(gatewayData);
            gateway.save((err,gatewayResp)=>{
                if(err) throw err;
                callback(gatewayResp)
            })
            if(err) throw err;
        })
    }

 /**
   * getGatewayDtls:The below function 
   * is used to retrieve Gateway details for
   * a particular pond 
   * @param:pondid
   * REST_URL:http://localhost:8080/aqua/api/gatewayRoutes/getGatewayDtls
   */

    this.getGatewayDtls=function(pondid,callback){
        let query=gatewayModel.find({}).select('gatewayserialnum');
        query.where({ponddtlsid:pondid});
        query.exec((err,gatewayResp)=>{
            callback(gatewayResp);
        })
    }

}
gatewayService = new gatewayService();

module.exports = gatewayService;