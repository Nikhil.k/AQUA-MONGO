var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var ponddtlsSchema = new Schema({
	'pondname' : String,
	'usermstid' :String,
	'ponddescr' : String,
	'pondstatus' : String,
	'location' : Array
},{collection:'ponddtls',versionKey: false});

module.exports = mongoose.model('ponddtls', ponddtlsSchema);
