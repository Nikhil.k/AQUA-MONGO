var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var sensorinfoSchema = new Schema({
	'sensorname' : String,
	'sensorserialnum' : String,
	'sensortype' : String,
	'gatewayid' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'gateway'
	},
	'sensorvalue' : String
},{collection:'sensorinfo',versionKey: false});

module.exports = mongoose.model('sensorinfo', sensorinfoSchema);
