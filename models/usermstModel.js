var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var usermstSchema = new Schema({
	'username' : String,
	'password' : String,
	'emailid' : String,
	'mobilenum' : Number
},{collection:'usermst',versionKey: false});

module.exports = mongoose.model('usermst', usermstSchema);
