var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var loginSchema = new Schema({
	'username' : String,
	'password' : String,
	'lastlogindatetime' : Date,
	'retriedattempts' : Number,
	'usermstid' : String
},{collection:'login',versionKey: false});

module.exports = mongoose.model('login', loginSchema);
