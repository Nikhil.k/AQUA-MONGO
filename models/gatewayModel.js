var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var gatewaySchema = new Schema({
	'gatewayserialnum' : String,
	'ponddtlsid':String,
	'sensors' : Array,
	'status' : String
},{collection:'gateway',versionKey: false});

module.exports = mongoose.model('gateway', gatewaySchema);
